<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEnd\HomeController@index')->name('home');
Route::get('/schools', 'FrontEnd\SchoolController@index')->name('schools');
Route::get('/universities', 'FrontEnd\UniversityController@index')->name('universities');
Auth::routes();

Route::get('/staff', 'Staff\HomeController@index')->name('staff');
Route::get('/staff/school/add', 'Staff\AddSchoolController@index')->name('addSchool');
Route::get('/staff/school', 'Staff\SchoolListController@index')->name('schoolList');
Route::post('/staff/school/public/{id}', 'Staff\SchoolListController@updateStatus')->name('public');
Route::post('/staff/school/add', 'Staff\AddSchoolController@addSchool')->name('addSchoolPost');

