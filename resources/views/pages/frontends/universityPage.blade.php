@extends('layouts.frontends.master')

@section('title')
    {{ __('School | ISchool') }}
@stop

@section('content')

    <h1 class="text-myColor ml-3">All Universities</h1> 
    <div class="row mt-3 mb-5">
    @foreach ($schools as $school)
    <div class="col-lg-3 mb-3">
        <div class="card">
            <img class="card-img-top" style="height: 200px" src={{ $school->school_image }} 
                alt="">
            <div class="card-body">
                <h3 class="card-title text-myColor">{{ $school->school_name }}</h3>
                <p class="card-text">{{ $school->school_desc }}</p>
                <button class="btn btn-primary bg-myColor">Check Now</button>
            </div>
        </div>
    </div>
    @endforeach
@stop

