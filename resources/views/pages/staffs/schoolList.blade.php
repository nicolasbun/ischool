@extends('layouts.staffs.master')

@section('title')
    {{ __('School List | Staff') }}
@stop

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-myColor">{{ __('All School List') }}</div>
                <div class="card-body">
                    <table class="table table-responsive-lg">
                        <thead>
                            <th>School Name</th>
                            <th>School Type</th>
                            <th>Added Date</th>
                            <th>Updated Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($schools as $school)
                                <tr>
                                    <td>{{ $school->school_name }}</td>
                                    <td>{{ ($school->school_type == 0) ? 'HighSchool' : 'University' }}</td>
                                    <td>{{ $school->created_at }}</td>
                                    <td>{{ $school->updated_at }}</td>
                                    <td> {{ ($school->status == 0) ? 'Close' : 'Published' }} </td>
                                    <td>
                                        <form method="POST" action={{ route('public', ['id' => $school->id]) }} >
                                            @csrf
                                    @if ($school->status == 0)
                                        <button type="submit" class="btn btn-primary bg-myColor">Public</button>
                                    @else
                                        <button type="submit" class="btn btn-danger">Close</button>
                                    @endif
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

