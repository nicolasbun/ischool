@extends('layouts.staffs.master')

@section('title')
    {{ __('Add School | Staff') }}
@stop

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-myColor">{{ __('Add School') }}</div>
                <div class="card-body">
                    <form method="POST" action={{ route('addSchoolPost') }} enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <img id='school_image' class="img-fluid img-thumbnail" src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fmoorestown-mall.com%2Fnoimage.gif&f=1&nofb=1" alt="">
                                <input id="schoolImage" name='image' type="file" class="mt-2" onchange="onImageChange(this)" />
                            </div>
                            <div class="col-lg-6">
                                <div class="col-lg-12 form-group">
                                    <label for="schoolName">School Name</label>
                                    <input type='text' name="schoolName" id="schoolName" class='form-control' />
                                </div>
                                <div class="col-lg-12 form-group">
                                    <label for="schoolName">School Address</label>
                                    <input type='text' id="schoolAddress" name='schoolAddress' class='form-control' />
                                </div>
                                <div class="col-lg-12 form-group">
                                    <label for="schoolName">School Type</label>
                                    <select name="schoolType" class="form-control" defaultValue='0'>
                                        <option value='0'>High School</option>
                                        <option value='1'>University</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group">
                            <label for="">Description</label>
                            <textarea class="form-control" name="schoolDesc" id="" cols="30" rows="10" placeholder="Enter description"></textarea>
                        </div>
                        <div class='col d-flex'>
                            <button type="submit" class="btn btn-primary bg-myColor ml-auto">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $('#schoolImage').change(function(){
            var input = this;
            var url = $(this).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
            {
                var reader = new FileReader();

                reader.onload = function (e) {
                $('#school_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            else
            {
            $('#img').attr('src', '/assets/no_preview.png');
            }
        });
    </script>
@endpush
