@extends('layouts.defaultLayout')

@section('body')
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="/">
            <img class="img-fluid mr-2" src="/images/logo.jpg" 
                width="30" height="30" alt="">
            ISchool
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse container navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link bg-myColor" href="#">{{__('Cities/Provinces')}}</a>
              </li>
              <li class="nav-item">
                <a class="nav-link bg-myColor" href={{ route('schools') }}>Schools</a>
              </li>
              <li class="nav-item">
                <a class="nav-link bg-myColor" href={{ route('universities') }}>Universities</a>
              </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link bg-myColor" href={{ route('staff') }}>Staff</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="row mt-5">
      <div class="col-lg-2 col-md-4 col-sm-4 ml-5 mb-5">
        <div class='card'>
          <div class="header">
            <a href="/admin/" class="btn btn-primary form-control bg-myColor">Dashboard</a>
          </div>
          <div class="card-body">
              <div class="row justify-content-center">
                  <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fpsdgraphics.com%2Ffile%2Fuser-icon.jpg&f=1&nofb=1" class="img-fluid" alt="">
                  <a href="/staff/" class="btn btn-primary bg-myColor form-control mt-2 ml-2 mr-2">My Profile</a>
                  <a href={{ route('addSchool') }} class="btn btn-primary bg-myColor form-control mt-2 ml-2 mr-2">Add School</a>
                  <a href={{ route('schoolList') }} class="btn btn-primary bg-myColor form-control mt-2 ml-2 mr-2">School List</a>
                  <form method="POST" action={{ route('logout') }} class="w-100">
                    <button class="btn btn-light form-control mt-2">Logout</button>
                  </form>
                </div>
          </div>
        </div>      
      </div>
      <div class="container col-lg-9 col-md-7 col-sm-7">
        @yield('content')
      </div>
    </div>

@stop
@section('footer')
    
@stop