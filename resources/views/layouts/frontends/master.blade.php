@extends('layouts.defaultLayout')

@section('body')
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand bg-myColor" href="/">
            <img class="img-fluid mr-2" src="/images/logo.jpg" 
                width="30" height="30" alt="">
            ISchool
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse container navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link bg-myColor" href="#">{{__('Cities/Provinces')}}</a>
              </li>
              <li class="nav-item">
                <a class="nav-link bg-myColor" href={{ route('schools') }}>Schools</a>
              </li>
              <li class="nav-item">
                <a class="nav-link bg-myColor" href={{ route('universities') }}>Universities</a>
              </li>
            </ul>
            <ul class="navbar-nav ml-auto">
            @guest
                <li class="nav-item">
                  <a class="nav-link bg-myColor" href={{ route('login') }}>Staff</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link bg-myColor" href={{ route('login') }}>Login</a>
                </li>
            @else
              <li class="nav-item">
                <a class="nav-link bg-myColor" href={{ route('login') }}>Staff</a>
              </li>
            @endguest
            </ul>
        </div>
    </nav>

    <div class="card">
        <img class="img-fluid w-100 card-img"  src="https://storage-prtl-co.imgix.net/endor/organisations/19446/covers/1582044031_hull_cover01.jpg?h=480&w=1920&fit=crop&auto=format,compress" alt="">
        <div class="card-img-overlay justify-content-center align-items-center d-flex">
          
        </div>
    </div>

    <div class="container mt-5">
        @yield('content')
    </div>
@stop
@section('footer')
    
@stop