<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
    use SoftDeletes;

    protected $date = ['deleted_at'];

    protected $fillable = [
        'school_name',
        'school_desc',
        'school_email',
        'school_image',
        'school_address',
        'school_type',
        'user_id'
    ];
}
