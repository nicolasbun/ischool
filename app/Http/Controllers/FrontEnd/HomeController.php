<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {
    public function index() {

        $popSchool = \App\School::where('user_id', Auth::id())->where('status', 1)->where('school_type', 0)->whereNull('deleted_at')->limit(4)->get();
        $popUniversity = \App\School::where('user_id', Auth::id())->where('status', 1)->where('school_type', 1)->whereNull('deleted_at')->limit(4)->get();

        return view('pages.frontends.homePage')->with('popSchool', $popSchool)->with('popUniversity', $popUniversity);
    }
}
