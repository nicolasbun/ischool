<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UniversityController extends Controller
{
    public function index() {
        $schools = \App\School::where('user_id', Auth::id())->where('status', 1)->where('school_type', 1)->whereNull('deleted_at')->get();
        return view('pages.frontends.universityPage')->with('schools', $schools);
    }
}
