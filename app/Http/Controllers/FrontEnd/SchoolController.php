<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SchoolController extends Controller
{
    public function index() {
        $popSchool = \App\School::where('user_id', Auth::id())->where('status', 1)->where('school_type', 0)->whereNull('deleted_at')->get();
        return view('pages.frontends.schoolPage')->with('schools', $popSchool);
    }
}
