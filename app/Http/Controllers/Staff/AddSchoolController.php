<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\School;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AddSchoolController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('pages.staffs.addSchool');
    }

    public function upload_post_image(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
  
        return back()->with('success','You have successfully upload image.');
   
    }

    public function addSchool(Request $request) {
        $imageName = "/images/" . Auth()->id() . "_" . time().'.'. $request->image->extension();  
        $request->image->move(public_path('images'), Auth()->id() . "_" . time().'.'. $request->image->extension());
        

        $school = new School([
            "school_name" => $request->get('schoolName'),
            "school_type" => $request->get('schoolType'),
            "school_address" => $request->get('schoolAddress'),
            "school_desc" => $request->get('schoolDesc'),
            "school_email" => "",
            "school_image" => $imageName,
            "user_id" => Auth()->id()
        ]);

        $school->save();

        return redirect()->route('schoolList');
    }
}
