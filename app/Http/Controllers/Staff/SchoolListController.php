<?php

namespace App\Http\Controllers\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\School;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SchoolListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {

        $schools = \App\School::where("user_id", Auth()->id())->get();
        return view('pages.staffs.schoolList')
            ->with("schools", $schools);
    }

    public function updateStatus($id) {
        $school = \App\School::where("user_id", Auth()->id())->find($id);
        if ($school->status == 0) $school->status = 1;
        else $school->status = 0;

        $school->save();

        return back();
    }
}
